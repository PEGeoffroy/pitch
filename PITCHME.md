# NIGHT

![night_logo](images/logo_night.png)

---

### Résumé

NIGHT (pour NIGHT Is a Gamemaster Heroic Toolbox) a pour ambition de devenir une boîte à outil multi-plateforme pour les meneurs de jeux.

+++

### Un utilisateur doit pouvoir :

@ol

- Se connecter / Se déconnecter
- Créer une partie
- Inviter d’autres utilisateurs
- Rejoindre une partie
- Créer un personnage (CRUD) en tant que joueur invité sur une partie
- Créer un ou des personnages (CRUD) en tant que meneur sur une partie

@olend

---

### Un administrateur doit pouvoir :

@ol

- Accepter l’inscription des visiteurs
- Gérer les utilisateurs (RUD)

@olend

+++

### Un visiteur doit pouvoir :

@ol

- Parcourir le site, lire la description du projet
- S’inscrire

@olend

---

### Technologies

@ol

- Node.JS
- Express

@olend

---

### La route

```js
GamesRouter.route('/')

  // Get all games
  .get(async (req, res) => {
    let allGames = await Games.getAll(req.query.max);
    res.json(checkAndChange(allGames));
  })

app.use(config.rootAPI + 'games', GamesRouter); // "rootAPI": "/api/v1/"
```

+++

### La méthode de classe

```js
  static getAll(max) {
    return new Promise((next) => {
      if (max != undefined && max > 0) {
        db.query('SELECT * FROM games LIMIT 0, ?', [parseInt(max)])
          .then((result) => next(result))
          .catch((err) => next(err))
      } else if (max != undefined) {
        next(new Error('Wrong max value'))
      } else {
        db.query('SELECT * FROM games')
          .then((result) => next(result))
          .catch((err) => next(err))
      }
    })
  }
```

+++

## Fonctions utiles

```js
exports.isError = (err) => {
    return err instanceof Error;
}

exports.checkAndChange = (obj) => {
    return this.isError(obj) ? this.error(obj.message) : this.success(obj);
}
```

+++

### L'appel asynchrone du front

```js
let url = `http://localhost:8082/api/v1/members`;
let button = document.getElementById('fetch');

async function asyncCall(url) {
    let response = await fetch(url);
    let data = await response.json();

    return data;
};

button.addEventListener("click", function () {
    asyncCall(`${url}/`)
        .then(game => console.info(game.name))
    i++;
}, false);
```

---

### Conception
